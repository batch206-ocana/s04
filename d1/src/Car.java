public class Car {

    //properties or attributes : the characteristics of the object the class will create.
    //constructor : method to create the object and instantiate with its initialized values
    //getters and setters : are method to get the values of an objects properties or set them.
    //methods : actions that an object can perform or do.


    //public access : the variable/property in the class is accessible anywhere in the application
    //private access : limits the access and ability to get or set a variable/method to only within its own class
    //getters : methods that returns the value of a property
    //setters : methods that allow us to set the value of a property
    private String make;

    private String brand;

    private int price;

    private Driver carDriver;

    public Car(){
        this.carDriver = new Driver();
    }

    //constructor
    public Car(String make, String brand, int price, Driver driver){
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    //getters and setters
    public String getMake(){
        //this keyword refers to the object/instance where the constructor or setter/getter is.
        return this.make;
    }
    public void setMake(String makeParams){
        this.make = makeParams;
    }

    //set a property as read-only. This means that the value of the property can only be get buy not updated
    public String getBrand(){
        return this.brand;
    }

    public int getPrice(){
        return this.price;
    }

    public void setPrice(int priceParams){
        this.price = priceParams;
    }

    //method
    //void means that the function does not return anything, because in java, a method's return datatype must also be declared
    public void start(){
        System.out.println("Vroom! Vroom!");
    }

    //classes have relationship
        //composition allows modelling objects to be made up to other objects. Classes can have instances of other classes.

    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    //custom method to retrieve the car driver's name
    public String getCarDriverName(){
        return carDriver.getName();
    }

}
