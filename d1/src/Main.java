public class Main {
    public static void main(String[] args) {

        //instance is an object created from a class and each instance of a class should be independent of one another
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Car car4 = new Car();

        Driver driver1 = new Driver("Alejandro",25);
        Car car5 = new Car("Vios","Toyota",1500000,driver1);

        car1.start();
        car2.start();
        car3.start();
        car4.start();
        car5.start();

        System.out.println(car1.getMake());
        System.out.println(car5.getMake());

        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        System.out.println(car1.getBrand());
        car1.setPrice(2000000);
        System.out.println(car1.getPrice());

        System.out.println(car5.getCarDriver().getName());
        Driver newDriver = new Driver("Antonio",25);
        car5.setCarDriver(newDriver);
        System.out.println(car5.getCarDriverName());

        Animal animal1 = new Animal("Jedd","Brown");
        System.out.println(animal1.call());;
    }
}